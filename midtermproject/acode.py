#imports needed libraries and functions for movement of robot in RViz
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
try:
        from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
        from math import pi, fabs, cos, sqrt
        
        tau = 2.0 * pi

        def dist(p, q):
            return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#Initializes the moveit commander and the ROS node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
#Create both a robotcommander object as well as instantize its 
#view of its surroundings, basically creating the scene around it
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
#Create joint group for ur5 in our case called "manipulator"
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
#Displaying the trajectory on RViz
display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
)
#Move robot joints to better starting position by turning
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8 # turn of -pi/4
joint_goal[2] = 0
joint_goal[3] = -tau / 4 # turn of -pi/2
joint_goal[4] = 0
joint_goal[5] = tau / 6  # turn of pi/3

#Clear the move groups targets so the next pose
#doesn't get influenced by this one
def stopping(data):
	move_group.stop()
	move_group.clear_pose_targets()


def startpos():
	#Starting position of robot end effector in XYZ plane
	pose_goal = geometry_msgs.msg.Pose()
	pose_goal.orientation.w = 1.0
	pose_goal.position.x = 0.5
	pose_goal.position.y = 0.5
	pose_goal.position.z = 0.2
	#Move the robot so the end effector is at the posed defined above
	move_group.set_pose_target(pose_goal)
	#Boolean value which checks if its reached the position above and
	#then stop moving the robot
	success = move_group.go(wait=True)
	stopping(success)

#Define the bottom right of the A
startpos()

#Define the top of the A, in between the y positions, x unchanged
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.25
pose_goal.position.z = 0.4
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)
#Define the bottom left  of the A, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.08
pose_goal.position.z = 0.2
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)

#Define the middle of the line between top and bottom right of the A
#x unchanged and start of the middle line of the A
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.125
pose_goal.position.z = 0.3
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)


#Define the middle of the line between top and bottom right of the A
#x unchanged and end of the middle line of the A
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.375
pose_goal.position.z = 0.3
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)

#Define the top left of the L, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.08
pose_goal.position.z = 0.5
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)

#Define the bottom left  of the L, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.08
pose_goal.position.z = 0.2
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
move_group.stop()
stopping(success)


#Define the bottom right of the L, miroring the start of the robots path
startpos()

#Define the top right of the V, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.08
pose_goal.position.z = 0.5
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)


#Define the bottom middle of the V, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.25
pose_goal.position.z = 0.2
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)

#Define the top right of the L, miroring the start of the robots path
#x,z same as start, y incrememnted
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.5
pose_goal.position.z = 0.5
#Move the robot so the end effector is at the posed defined above
move_group.set_pose_target(pose_goal)
#Boolean value which checks if its reached the position above and
#then stop moving the robot
success = move_group.go(wait=True)
stopping(success)



